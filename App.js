/**
 * Aplicativo de exemplo de carregamento de imagens em grid.
 * @format
 * @flow
 */
import React, { useEffect } from 'react';
import AppContainer from './src/StackNavigation';
import SplashScreen from 'react-native-splash-screen'

const App = () => {

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <AppContainer />
  );
};

export default App;