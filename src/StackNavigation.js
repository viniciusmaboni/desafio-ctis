import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import ImageGrid from './components/ImageGrid';
import ImageDetail from './components/ImageDetail';


const AppNavigator = createStackNavigator({
  Home: {
    screen: ImageGrid,
  },
  Detail: {
    screen: ImageDetail,
  },
},
  {
    initalRoute: 'Home',
  }
);

export default createAppContainer(AppNavigator);