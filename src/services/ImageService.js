
function getImageDataAsync() {
  return fetch('https://jsonplaceholder.typicode.com/photos')
    .then((response) => response.json())
    .then((responseJson) => {
      return responseJson;
    })
    .catch((error) => {
      console.error(error);
    });
}

export { getImageDataAsync };