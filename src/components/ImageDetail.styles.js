import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  image: {
    position: 'absolute',
    top: 0,
    left: 10,
    bottom: 0,
    right: 10,
  },
  conteiner: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  title: {
    marginTop: 30,
    marginLeft: 10,
    marginRight: 10,
    fontSize: 16,
    fontWeight: "bold"
  }
});


export default styles;