import React, { useState, useEffect } from 'react';
import { FlatList, ActivityIndicator, View, Dimensions } from 'react-native';
import { getImageDataAsync } from '../services/ImageService';
import Thumbnail from './Thumbnail';
import styles from './ImageGrid.styles';
import PropTypes from 'prop-types';


const ImageGrid = ({ navigation }) => {
  const [data, setData] = useState({ imgs: [] })

  useEffect(() => {
    const fetchData = async () => {
      const result = await getImageDataAsync();
      setData(result);
    };
    fetchData();

  }, []);

  const { width } = Dimensions.get('window');
  const numberGrid = 3;
  const itemWidth = width / numberGrid;

  const renderItem = ({ item }) => (
    <Thumbnail thumbnailUrl={item.thumbnailUrl}
      onPress={() => {
        navigation.navigate('Detail', { item: item })
      }
      }
      size={itemWidth}
    />
  );

  if (data.length > 0) {
    return (
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        numColumns={4}
      />

    );
  }
  else {
    return renderLoading();
  }
}

const renderLoading = () => (
  <View style={styles.loader}>
    <ActivityIndicator size="large" color="#0000ff" />
  </View>
)

ImageGrid.navigationOptions = {
  title: 'Home',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

ImageGrid.propTypes = {
  navigation: PropTypes.any
}

export default ImageGrid;