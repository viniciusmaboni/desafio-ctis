import React from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const Thumbnail = ({ thumbnailUrl, onPress, size }) => {

  /**Estilo interno devida logica de tamanho dinamica */
  const styles = StyleSheet.create({
    image: {
      width: size, height: size, margin: 1
    },
  });


  return (
    <TouchableOpacity onPress={onPress}>
      <Image
        style={styles.image}
        source={{ uri: thumbnailUrl }}
      />
    </TouchableOpacity>
  );
}

Thumbnail.propTypes = {
  thumbnailUrl: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  size: PropTypes.number.isRequired
}

export default Thumbnail;