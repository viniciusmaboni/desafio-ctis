import React, { useState } from 'react';
import { Image, ActivityIndicator, View, Text } from 'react-native';
import styles from './ImageDetail.styles';
import PropTypes from 'prop-types';

const ImageDetail = ({ navigation }) => {

  let [loading, setLoading] = useState(true);

  let item = navigation.getParam('item');
  return (
    <View style={styles.conteiner}>
      <Text style={styles.title}>{item.title}</Text>
      <Image
        style={styles.image}
        source={{ uri: item.url }}
        resizeMode="contain"
        onLoadEnd={() => setLoading(false)}
      />
      <ActivityIndicator size="large" color="#0000ff" animating={loading} />
    </View>
  )
}

ImageDetail.navigationOptions = {
  title: 'Visualização',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

ImageDetail.propTypes = {
  navigation: PropTypes.any
}

export default ImageDetail;